/*
  Simple RSSI Antenna Tracker
RELATIVE
*/

#include <Arduino.h>
#include <Servo.h>
#include <Timer.h>
#include <SPI.h>
#include <avr/power.h>

// En 3 wire SPI -> Ajouter une résistance de 10kOhm sur MOSI (ici pin 11)

// MOSI --- 10kOhm -------------- DATA
//                     |
// MISO ---------------|

/* 
 *  For testing different curves
 *  All new curves are more precise
 *  and maintain lock better than old RELATIVE
 *  
 *  Uncomment one 
 */
//#define EXPONENTIAL       // OK
//#define RELATIVE          // old, don't use
#define SIGMOID // Best
//#define PROPORTIONAL      // twitchy

#define SIGMOID_SLOPE 1
#define SIGMOID_OFFSET 4

#if !defined(EXPONENTIAL) ^ !defined(SIGMOID) ^ !defined(PROPORTIONAL) ^ !defined(RELATIVE)
// all good
#else
#error "Please define ONE tracking curve: EXPONENTIAL, SIGMOID, PROPORTIONAL, RELATIVE"
#endif

/*
   Pin mapping
   You can change these pins, just make sure that
   RSSI pins are analog inputs
   and servo pin supports PWM
*/

#define PAN_SERVO_PIN 5 // Pan servo pin

/*
   There is going to be some difference in receivers,
   one is going to be less sensitive than the other.
   It will result in slight offset when tracker is centered on target
   Find value that evens out RSSI
 */
#define RSSI_OFFSET_RIGHT 0
#define RSSI_OFFSET_LEFT 0

/*
   MIN and MAX RSSI values corresponding to 0% and 100% from receiver
   See serial monitor
*/
#define RSSI_MAX 400
#define RSSI_MIN 120

/*
   Safety limits for servo
   Find values where servo doesn't buzz at full deflection
*/
#define SERVO_MAX 180
#define SERVO_MIN 0

/*
 * Servo 'speed' limits
 * MAX and MIN step in degrees
 */
#define SERVO_MAX_STEP 5
#define SERVO_MIN_STEP 0.09 // prevents windup and servo crawl

/*
   Center deadband value!
   Prevents tracker from oscillating when target is in the center
   When using SIGMOID or EXPONENTIAL you can set this almost to 0
*/
#define DEADBAND 5

/*
   Depending which way around you put your servo
   you may have to change direction
   either 1 or -1
*/
#define SERVO_DIRECTION 1

#define FIR_SIZE 10
#define LAST FIR_SIZE - 1

uint16_t rssi_left_array[FIR_SIZE];
uint16_t rssi_right_array[FIR_SIZE];

float anglePan = 90;
boolean debug = false;

Timer timer;
Servo servoPan;

const byte GET = 0x3f << 2;   // Rapidfire GET command
const byte FW_VERSION = 0x46; // Firmware version register
// Return X | Y | Z | x | y | z -> XYZ = rapidfire OLED fw version & xyz = rapidfire core fw version

const byte VOLTAGE = 0x46; // Voltage register
// Return mV LSB | mV MSB

const byte RSSI = 0x52; // RSSI Values register
// Return Raw rx1 LSB | Raw rx1 MSB | Raw rx2 LSB | Raw rx2 MSB | Scaled rx1 LSB | Scaled rx1 MSB | Scaled rx2 LSB | Scaled rx2 MSB

const byte BUZZER = 0x3e << 2;  // Rapidfire BUZZER command
const byte SHORT_BUZZER = 0x53; // Short buzzer register

const byte SET = 0x3d << 2; // Rapidfire SET command

const byte OSD_USER_TEXT = 0x54; // OSD User Text register (max 25 char)

const byte RX_MODE = 0x4d;        // RX MODE register
const byte RX_MODE_BOTH = 0x00;   // BOTH
const byte RX_MODE_UPPPER = 0x01; // UPPER
const byte RX_MODE_LOWER = 0x02;  // LOWER

const byte CHANNEL = 0x43; // CHANNEL register -> range between 0x01..0x08

const byte BAND = 0x42; // BAND register
const byte FATSHARK_BAND = 0x01;
const byte RACE_BAND = 0x02;
const byte BOSCAM_E_BAND = 0X03;
const byte BOSCAM_B_BAND = 0x04;
const byte BOSCAM_A_BAND = 0x05;
const byte LOW_RACE_BAND = 0x06;
const byte X_BAND = 0x07;

const byte RF_MODE = 0x44;        // RF MODE Register
const byte RF_MODE_RF1 = 0x00;    // RapidFire #1
const byte RF_MODE_RF2 = 0x01;    // RapidFire #2
const byte RF_MODE_LEGACY = 0x02; // Legacy

const byte SET_OSD = 0x0d << 2; // Rapidfire SET_OSD Mode command

const byte OSD_MODE = 0x4f; // OSD MODE Register
// 0 - Off
// 1 - Default
// 2 - LockOnly
// 3 - UserText
// 4 - RSSIBars
// 5 - LockAndStandard
// 6 - RSSIBarsLite
// 7 - Internal use
// 8 - Internal use
// 9- Unique ID
// 10 - Long Range

uint16_t rawRssi1 = 0;
uint16_t rawRssi2 = 0;
uint16_t scaledRssi1 = 0;
uint16_t scaledRssi2 = 0;

void rapidfireBuzzer()
{
  Serial.println("BUZZER");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x53);
  SPI.transfer(0x3e);
  SPI.transfer(0x00);
  SPI.transfer(0x91);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void getFirmwareVersion()
{
  byte Len;
  byte Csum;
  byte Xversion;
  byte Yversion;
  byte Zversion;
  byte xversion;
  byte yversion;
  byte zversion;

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x46);
  SPI.transfer(0x3f);
  SPI.transfer(0x00);
  SPI.transfer(0x85);

  Len = SPI.transfer(0x00);
  Csum = SPI.transfer(0x00);
  Xversion = SPI.transfer(0x00);
  Yversion = SPI.transfer(0x00);
  Zversion = SPI.transfer(0x00);
  xversion = SPI.transfer(0x00);
  yversion = SPI.transfer(0x00);
  zversion = SPI.transfer(0x00);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);

  delay(500);

  Serial.print("Lenght : ");
  Serial.println(Len, HEX);

  Serial.print("Checksum : ");
  Serial.println(Csum, HEX);

  Serial.print("RF Oled FW version : ");
  Serial.print(Xversion, HEX);
  Serial.print(".");
  Serial.print(Yversion, HEX);
  Serial.print(".");
  Serial.println(Zversion, HEX);

  Serial.print("RF Core FW version : ");
  Serial.print(xversion, HEX);
  Serial.print(".");
  Serial.print(yversion, HEX);
  Serial.print(".");
  Serial.println(zversion, HEX);
}

void getVoltage()
{
  byte Len;
  byte Csum;
  byte mvLsb;
  byte mvMsb;

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x56);
  SPI.transfer(0x3f);
  SPI.transfer(0x00);
  SPI.transfer(0x95);

  Len = SPI.transfer(0x00);
  Csum = SPI.transfer(0x00);
  mvLsb = SPI.transfer(0x00);
  mvMsb = SPI.transfer(0x00);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);

  Serial.print("Lenght : ");
  Serial.println(Len, HEX);

  Serial.print("Checksum : ");
  Serial.println(Csum, HEX);

  Serial.print("mV MSB : ");
  Serial.println(mvMsb);

  Serial.print("mV LSB : ");
  Serial.println(mvLsb);
}

void setChannel()
{
  Serial.println("SET CHANNEL");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x43);
  SPI.transfer(0x3d);
  SPI.transfer(0x01);
  SPI.transfer(0x82);
  SPI.transfer(0x01);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void setBand()
{
  Serial.println("SET BAND");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x42);
  SPI.transfer(0x3d);
  SPI.transfer(0x01);
  SPI.transfer(0x81);
  SPI.transfer(0x07);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void setRapidfireMode()
{
  Serial.println("SET RAPIDFIRE MODE");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x44);
  SPI.transfer(0x3d);
  SPI.transfer(0x01);
  SPI.transfer(0x82);
  SPI.transfer(0x01);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void setOsdMode()
{
  Serial.println("SET OSD MODE");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x4f);
  SPI.transfer(0x0d);
  SPI.transfer(0x01);
  SPI.transfer(0x63);
  SPI.transfer(0x03);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void setOsdUserText()
{
  Serial.println("SET OSD USER TEXT");
  delay(100);

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(160000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x54);
  SPI.transfer(0x3d);
  SPI.transfer(0x05);
  SPI.transfer(0x8a);
  SPI.transfer(0x48);
  SPI.transfer(0x65);
  SPI.transfer(0x6c);
  SPI.transfer(0x6c);
  SPI.transfer(0x6f);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);
}

void readRssi()
{
  Serial.println("Read RSSI");

  uint8_t len;
  uint8_t csum;

  clock_prescale_set(clock_div_2);
  SPI.beginTransaction(SPISettings(150000, MSBFIRST, SPI_MODE0));

  digitalWrite(PIN_SPI_SS, LOW);

  SPI.transfer(0x52);
  SPI.transfer(0x3f);
  SPI.transfer(0x00);
  SPI.transfer(0x91);

  len = SPI.transfer(0x00);
  csum = SPI.transfer(0x00);

  rawRssi1 = ((rawRssi1 & 0xff) >> 8) | (rawRssi1 << 8);
  rawRssi2 = ((rawRssi2 & 0xff) >> 8) | (rawRssi2 << 8);
  scaledRssi1 = ((scaledRssi1 & 0xff) >> 8) | (scaledRssi1 << 8);
  scaledRssi2 = ((scaledRssi2 & 0xff) >> 8) | (scaledRssi2 << 8);

  rawRssi1 = SPI.transfer16(0x00);

  rawRssi2 = SPI.transfer16(0x00);

  scaledRssi1 = SPI.transfer16(0x00);

  scaledRssi2 = SPI.transfer16(0x00);

  digitalWrite(PIN_SPI_SS, HIGH);
  SPI.endTransaction();
  clock_prescale_set(clock_div_1);

  delay(100);

  rawRssi1 = (rawRssi1 >> 8) | (rawRssi1 << 8);
  rawRssi2 = (rawRssi2 >> 8) | (rawRssi2 << 8);
  scaledRssi1 = (scaledRssi1 >> 8) | (scaledRssi1 << 8);
  scaledRssi2 = (scaledRssi2 >> 8) | (scaledRssi2 << 8);

  Serial.print("Len : ");
  Serial.println(len, DEC);

  Serial.print("Csum : ");
  Serial.println(csum, DEC);

  Serial.print("Raw RSSI1 : ");
  Serial.println(rawRssi1, DEC);

  Serial.print("Raw RSSI2 : ");
  Serial.println(rawRssi2, DEC);

  Serial.print("Scaled RSSI1 : ");
  Serial.println(scaledRssi1, DEC);

  Serial.print("Scaled RSSI2 : ");
  Serial.println(scaledRssi2, DEC);
}

float limit(float lowerLimit, float upperLimit, float var)
{
  return min(max(var, lowerLimit), upperLimit);
}

void movePanBy(float angle)
{
  anglePan += angle;
  anglePan = limit(SERVO_MIN, SERVO_MAX, anglePan);
  servoPan.write(anglePan);
}

uint16_t avg(uint16_t samples[], uint8_t n)
{
  uint32_t summ = 0;
  for (uint8_t i = 0; i < n; i++)
  {
    summ += samples[i];
  }

  return uint16_t(summ / n);
}

void mainLoop()
{
  uint16_t avgLeft = max(avg(rssi_left_array, FIR_SIZE) + RSSI_OFFSET_LEFT, RSSI_MIN);
  uint16_t avgRight = max(avg(rssi_right_array, FIR_SIZE) + RSSI_OFFSET_RIGHT, RSSI_MIN);

  //  If avg RSSI is above 90%, don't move
  //  if ((avgRight + avgLeft) / 2 > 360) {
  //    return;
  //  }

  /*
     the lower total RSSI is, the lower deadband gets
     allows more precise tracking when target is far away
  */
  uint8_t dynamicDeadband = (float(avgRight + avgLeft) / 2 - RSSI_MIN) / (RSSI_MAX - RSSI_MIN) * DEADBAND;

  // if target is in the middle, don't move
  if (abs(avgRight - avgLeft) < dynamicDeadband)
  {
    return;
  }

  float ang = 0;

  // move towards stronger signal
  if (avgRight > avgLeft)
  {
#if defined(EXPONENTIAL)
    float x = float(avgRight - avgLeft);
    x = x * x / 500;
    ang = x * SERVO_DIRECTION * -1;
#endif

#if defined(RELATIVE)
    ang = float(avgRight / avgLeft) * (SERVO_DIRECTION * -1);
#endif

#if defined(SIGMOID)
    float x = float(avgRight - avgLeft) / 10;
    x = SERVO_MAX_STEP / (1 + exp(-SIGMOID_SLOPE * x + SIGMOID_OFFSET));
    ang = x * SERVO_DIRECTION * -1;
#endif

#if defined(PROPORTIONAL)
    float x = float(avgRight - avgLeft) / 10;
    ang = x * SERVO_DIRECTION * -1;
#endif
  }
  else
  {
#if defined(EXPONENTIAL)
    float x = float(avgLeft - avgRight);
    x = x * x / 500;
    ang = x * SERVO_DIRECTION;
#endif

#if defined(RELATIVE)
    ang = float(avgLeft / avgRight) * SERVO_DIRECTION;
#endif

#if defined(SIGMOID)
    float x = float(avgLeft - avgRight) / 10;
    x = SERVO_MAX_STEP / (1 + exp(-SIGMOID_SLOPE * x + SIGMOID_OFFSET));
    ang = x * SERVO_DIRECTION;
#endif

#if defined(PROPORTIONAL)
    float x = float(avgLeft - avgRight) / 10;
    ang = x * SERVO_DIRECTION;
#endif
  }

  // upper and lower limit for angle step
  ang = (abs(ang) > SERVO_MAX_STEP ? SERVO_MAX_STEP * ang / abs(ang) : ang);
  ang = (abs(ang) < SERVO_MIN_STEP ? 0 : ang);

  // move servo by n degrees
  movePanBy(ang);

  if (debug)
  {
    //    Serial.print("RSSI%: ");
    //    Serial.print(map(avgLeft, RSSI_MIN, RSSI_MAX, 0, 100));
    //    Serial.print(", ");
    //    Serial.print(map(avgRight, RSSI_MIN, RSSI_MAX, 0, 100));

    // raw rssi values, use these for RSSI_MIN and RSSI_MAX
    Serial.print("Calibration - left: ");
    Serial.print(avgLeft);
    Serial.print(" right: ");
    Serial.print(avgRight);

    Serial.print(" servo-angle: ");
    Serial.println(anglePan);
  }
}

void advanceArray(uint16_t *samples, uint8_t n)
{
  for (uint8_t i = 0; i < n - 1; i++)
  {
    samples[i] = samples[i + 1];
  }
}

void measureRSSI()
{
  advanceArray(rssi_left_array, FIR_SIZE);
  advanceArray(rssi_right_array, FIR_SIZE);

  readRssi();

  rssi_left_array[LAST] = rawRssi1;
  rssi_right_array[LAST] = rawRssi2;
}

void setup()
{
  Serial.begin(9600);

  Serial.println("PASSAGE EN SPI");
  delay(2000);

  // Setup RapidFire in SPI Mode
  // To enable SPI mode, set CS1, CS2, CS3 high, and then within 100-400ms set them all low.
  Serial.println("Pin en output");
  pinMode(PIN_SPI_SCK, OUTPUT);
  pinMode(PIN_SPI_MOSI, OUTPUT);
  pinMode(PIN_SPI_SS, OUTPUT);

  Serial.println("Pin en High");
  digitalWrite(PIN_SPI_SCK, HIGH);
  digitalWrite(PIN_SPI_MOSI, HIGH);
  digitalWrite(PIN_SPI_SS, HIGH);

  Serial.println("Delay 300");
  delay(300);

  Serial.println("Pin en Low");
  digitalWrite(PIN_SPI_SCK, LOW);
  digitalWrite(PIN_SPI_MOSI, LOW);
  digitalWrite(PIN_SPI_SS, LOW);

  Serial.println("Delay 500");
  delay(500);

  Serial.println("SPI ACTIF");

  servoPan.attach(PAN_SERVO_PIN);
  servoPan.write(90);

  // wipe array
  for (int i = 0; i < FIR_SIZE; i++)
  {
    rssi_right_array[i] = 0;
    rssi_left_array[i] = 0;
  }

  // start the SPI library:
  SPI.begin();

  // initalize the chip select pin:
  pinMode(PIN_SPI_SS, OUTPUT);
  digitalWrite(PIN_SPI_SS, HIGH);

  delay(500);

  while (!debug)
  {
    delay(1000);
    debug = true;
  }

  //getFirmwareVersion();
  //getVoltage();
  //setChannel();
  //setBand();
  //setRapidfireMode();
  //rapidfireBuzzer();
  //setOsdMode();
  //setOsdUserText();
  rapidfireBuzzer();

  timer.every(10000, mainLoop);
  timer.every(1000, measureRSSI);
}

void loop()
{
  timer.update();
}
